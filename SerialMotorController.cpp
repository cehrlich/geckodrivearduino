/*******************************************************************************

This is an example class of how to implement a class that controls multiple motors
and is using serial communication. Other ways of communication can be implemented easily
by writing a custom controller.

NOTES:

 - There is currently no serial reply when a motor command is finished (e.g. position is reached).
   Trying this made communication quite complicated so I dropped it.
 - This code was written for the Due where the size of an int is 4 bytes. Other platforms might require
   modifications here.



The MIT License (MIT)

Copyright (c) s2016 Christoph Ehrlich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

********************************************************************************/

#include "SerialMotorController.h"

SerialMotorController::SerialMotorController(Stream &port) : _port(port) {
}


/*
Set up the motor controller class. Computes some helper variables and so on. Call this after setting the motor
properties in the setup-function of the sketch.
*/
void SerialMotorController::setup() {
	// find fastest motor velocity
	float maxVelocity = 0;
	for(int i = 0; i < NUMBER_MOTORS; i++) {
		if(motors[i].maxVelocitySteps > maxVelocity) {
			maxVelocity = motors[i].maxVelocitySteps;
		}
	}

	// use this maximum velocity (s) to set the serial listening time (us) to reduce jitter in step timing
	if(maxVelocity > 0) {
		// 10% of minimum time between two steps (at max. velocity) and convert s to us
		serialListenTime = 1e5/maxVelocity;
	} else {
		serialListenTime = 100;
	}
}


/*
Check all registered motors for pending steps and execute them.
*/
void SerialMotorController::step() {
	int res;
	for(int i = 0; i < NUMBER_MOTORS; i++) {
		res = motors[i].checkStep();
		#ifdef SERIAL_DEBUG
			if(res != errNoStepPending) {
				String info = motors[i].getDebugInfo();
				info = "id: " + String(i) + ", " + info;
				DEBUG_PORT.println(info);
			}
		#endif
	}
}


/*
Listen on the serial port for new commands. The time spent listening is set in "setup" and depends of the
velocity of the fastest motor.
*/
void SerialMotorController::serialListen() {
	unsigned long startTime = micros();
	while(micros() - startTime < serialListenTime) {
		// timeout for command arrival
		// this should prevent incomplete commands (or lost bytes) to block the serial communication
		if((cmdStartTime != 0) && (micros() - cmdStartTime > cmdTimeout)) {
			cmdBufferPos = 0;
			cmdStartTime = 0;
		}
		if(_port.available() > 0) {
			// start timeout counter if it is not running
			if(cmdStartTime == 0) cmdStartTime = micros();
			// read byte
			byte inByte = _port.read();
			// did we recive a full command?
			if(cmdBufferPos == sizeof(cmd))
			{
				// if so, does it terminate on the proper byte?
				if(inByte == termByte) {
					#ifdef SERIAL_DEBUG
						DEBUG_PORT.println("received command: " + String(cmd.msg.cmd));
					#endif
					// succesfully received command, run appropriate handler
					serialCallHandler();
				}

				// reset variables: required for both cases (succesful or unsuccesful reception)
				cmdBufferPos = 0;
				cmdStartTime = 0;
			}
			else {
				// append byte to buffer
				// prevent writing to other memory sections
				if(cmdBufferPos < sizeof(cmd)) {
					cmd.bytes[cmdBufferPos++] = inByte;
				}
			}
		}
	}
}


/*
Send a multi-byte reply through the serial connection with length prefix and termination byte (included in length).
*/
void SerialMotorController::serialReply(byte byteArray[], unsigned int size) {
	unsigned int totSize = size + 1;	
	_port.write((byte*)&totSize, 4);
	_port.write(byteArray, size);
	_port.write(termByte);
}


/*
Send a single-byte reply through the serial connection with length prefix and termination byte (included in length).
*/
void SerialMotorController::serialReply(byte byteReply) {
	unsigned int size = 2;
	byte reply[2] = {byteReply, termByte};
	_port.write((byte*)&size, 4);
	_port.write(reply, 2);
}


/*
Call the appropriate handler function for the command in the command buffer.
*/
void SerialMotorController::serialCallHandler() {
	switch(cmd.msg.cmd) {
		case 0:
			serialStatus();
			break;
		case 1:
			serialMoveVelocity();
			break;
		case 2:
			serialMoveTo();
			break;
		case 3:
			serialMoveSteps();
			break;
		case 4:
			serialMoveStop();
			break;
		case 5:
			serialHome();
			break;
		case 10:
			serialInfo();
			break;
		case 11:
			serialSetMaxVelRel();
			break;
		case 12:
			serialSetMinPos();
			break;
		case 13:
			serialSetMaxPos();
			break;
		default:
			serialUnknownCommand();
	}
}


/*
Default handler for unknown commands.
*/
void SerialMotorController::serialUnknownCommand() {
	serialReply(errSerialUnknown);
}


/*
Check for valid motor ID.

RETURN:
	error code
*/
byte SerialMotorController::checkMotorId() {
	if(cmd.msg.id >= NUMBER_MOTORS) {
		serialReply(errSerialId);
		return errSerialId;
	} else return errSuccess;
}


/*
Serial command handler that prints the status of all motors. For return values, see StepperMotor::getStatus().
*/
void SerialMotorController::serialStatus() {
	for(int i = 0; i < NUMBER_MOTORS; i++) {
		motors[i].getStatus(&(statusPckg.msg[i]));
	}
	serialReply(statusPckg.bytes, sizeof(statusPckg));
}


/*
Serial command handler that prints information about all the motors.
*/
void SerialMotorController::serialInfo() {
	for(int i = 0; i < NUMBER_MOTORS; i++) {
		motors[i].getInfo(&(infoPckg.msg[i]));
	}
	serialReply(infoPckg.bytes, sizeof(infoPckg));
}


/*
Serial command handler to move a motor by a given number of steps. The sign of the step number defines the direction.

SERIAL PARAMTERES:
	[id - motor ID] [ival - position] {fval - relative velocity, default: 1}
*/
void SerialMotorController::serialMoveSteps() {
	if(checkMotorId() == errSuccess) {
		// set velocity to 1 if it is not set
		if(cmd.msg.fval == 0) cmd.msg.fval = 1;
		// run handler
		byte res = motors[cmd.msg.id].moveSteps(cmd.msg.ival, cmd.msg.fval);
		serialReply(res);
	}
}


/*
Serial command handler to move the motor to an absolute position in steps,

SERIAL PARAMTERS:
	[id - motor ID] [ival - position] {fval - relative velocity, default: 1}
*/
void SerialMotorController::serialMoveTo() {
	if(checkMotorId() == errSuccess) {
		// set velocity to 1 if it is not set
		if(cmd.msg.fval == 0) cmd.msg.fval = 1;
		// run handler
		byte res = motors[cmd.msg.id].moveTo(cmd.msg.ival, cmd.msg.fval);
		serialReply(res);
	}
}


/*
Serial command handler that moves the motor with a constant velocity. The sign of the velocity defines the direction
and its value is relative of the maximum velocity of the motor (between 0 and 1).

SERIAL PARAMETERS:
	[id - motor ID] {fval - relative velocity, default: 1}
*/
void SerialMotorController::serialMoveVelocity() {
	if(checkMotorId() == errSuccess) {
		// run handler
		byte res = motors[cmd.msg.id].moveVelocity(cmd.msg.fval);
		serialReply(res);
	}
}


/*
Emergency stop the motor.

SERIAL COMMAND:
	[id - motor ID]
*/
void SerialMotorController::serialMoveStop() {
	if(checkMotorId() == errSuccess) {
		byte res = motors[cmd.msg.id].stop();
		serialReply(res);
	}
}


/*
Home the motor.

SERIAL COMMAND:
	[id - motor ID]
*/
void SerialMotorController::serialHome() {
	if(checkMotorId() == errSuccess) {
		byte res = motors[cmd.msg.id].moveHome();
		serialReply(res);
	}
}


/*
Set maximum relative velocity. See StepperMotor::setMaxVelRel.

SERIAL COMMAND:
	[id - motor ID] [fval - maximum relative velocity]
*/
void SerialMotorController::serialSetMaxVelRel() {
	if(checkMotorId() == errSuccess) {
		byte res = motors[cmd.msg.id].setMaxVelRel(cmd.msg.fval);
		serialReply(res);
	}
}


/*
Set minimum position of the motor. See StepperMotor::setMinPos.

SERIAL COMMAND:
	[id - motor ID] [ival - minimum position]
*/
void SerialMotorController::serialSetMinPos() {
	if(checkMotorId() == errSuccess) {
		byte res = motors[cmd.msg.id].setMinPos(cmd.msg.ival);
		serialReply(res);
	}
}


/*
Set maximum position of the motor. See StepperMotor::setMaxPos.

SERIAL COMMAND:
	[id - motor ID] [ival - maximum position]
*/
void SerialMotorController::serialSetMaxPos() {
	if(checkMotorId() == errSuccess) {
		byte res = motors[cmd.msg.id].setMaxPos(cmd.msg.ival);
		serialReply(res);
	}
}