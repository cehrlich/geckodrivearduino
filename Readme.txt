Wiring
------

For general use, connect to the Arduino Due using the Native USB Port. This will not reset the Arduino
when connecting to it (except if you use 1200 bps). The native USB Port is the one next to the 
reset button.


Debug
-----

For debugging, additionally connect the Programming USB port and connect to it using the Arudino IDE's
Serial Monitor. Note that the Arduino will be reset at every connection.
Also uncomment the following line in Config.h:

// # define SERIAL_DEBUG

Note that actual physical stepping might be unreliable during debugging becuase of the delays introduced
by the additional serial communication for each step. When in doubt, disable the messages that are
sent per step and add additional messages as needed.