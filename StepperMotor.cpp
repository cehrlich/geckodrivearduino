/********************************************************************************

The logic to compute the waiting times follows the article by David Austin.
http://www.embedded.com/design/mcus-processors-and-socs/4006438/Generate-stepper-motor-speed-profiles-in-real-time
Additional helpful resources:
	http://www.atmel.com/images/doc8017.pdf for reference.
	http://www.ti.com/lit/an/slyt482/slyt482.pdf
	http://titania.ctie.monash.edu.au/papers/pj-stepper-motor-control.pdf
	http://forum.arduino.cc/index.php?topic=129868.15
	http://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=1881 (Calculations tab)


NOTES:

 - In the context of this software, a "step" is a pulse created by the board. How this translates to
   full steps / usteps of the motor depends on your controller. E.g. the GeckoDrive 203V produces 10 usteps
   per full step. How this translates to motion of the motor depends on its gearbox ratio and lead screw.

 - See the "StepperMotor.h" for some configuration variables.

 - The "turnMotorOn" and "turnMotorOff" methods are implemented but not currently used to disable the
   motor when not stepping.

 - If the motor is stopping without any obvious reason, one problem could be noisy limit switches.
   In that case, increasing the size of ccwLSBuffer and cwLSBuffer could help.


The MIT License (MIT)

Copyright (c) 2015 Christoph Ehrlich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

********************************************************************************/

#include "StepperMotor.h"

/*
Configures the pins used by the motor. Set them to the appropriate pin numbers.

INPUT:
	disPin		- disable pin
	dirPin		- direction pin
	stepPin		- stepping pin
	ccwLimitPin	- counter-clockwise limit switch pin (minimum extension of motor)
	cwLimitPin	- clowckwise limit switch pin (maximum extension of motor)
*/
void StepperMotor::configPins(int _disPin,
							  int _dirPin,
							  int _stepPin,
							  int _ccwLimitPin,
							  int _cwLimitPin) {
	disPin = _disPin;
	dirPin = _dirPin;
	stepPin = _stepPin;
	ccwLimitPin = _ccwLimitPin;
	cwLimitPin = _cwLimitPin;

	pinMode(disPin, OUTPUT);
	pinMode(dirPin, OUTPUT);
	pinMode(stepPin, OUTPUT);
	#ifdef USE_LIMITS
		pinMode(ccwLimitPin, INPUT_PULLUP);
		pinMode(cwLimitPin, INPUT_PULLUP);
	#endif

	digitalWriteDirect(stepPin, LOW);
	setDirection(currentDir);
	stepState = LOW;
	turnMotorOn();
}


/*
Configures the motor specifications. How to calcuate 

NOTE:
	travelRange is used to define the software limits for motor motion. A default value is provided in case
	software limits are not used.
	Also, when using a stepper that can rotate freely (without lead screw), the units of maxVelocity, maxAccel
	and stepsPerMM change and you have to make sure that everyting fits together (e.g. deg / s and steps / deg).
	How to calculate stepsPerMM is explained on the Thorlabs site referenced at the top.

INPUT:
	maxVelocity	- maximum velocity in mm/s
	maxAccel	- maximum acceleration in mm/s^2
	stepsPerMM	- microsteps per mm travel of the motor, default 0
	travelRange	- maximum travel range in mm
*/
void StepperMotor::configMotor(float _maxVelocity,
							   float _maxAccel,
							   float _stepsPerMM,
							   float _travelRange /* = 0 */
							   ) {
	maxVelocity = _maxVelocity;
	maxAccel = _maxAccel;
	stepsPerMM = _stepsPerMM;
	travelRange = _travelRange;
	
	// compute required parameters
	// max velocity in steps / s
	maxVelocitySteps = stepsPerMM * maxVelocity;
	// max acceleration in steps / s^2
	maxAccelSteps = stepsPerMM * maxAccel;
	posLimits[0] = 0;
	posLimits[1] = stepsPerMM * travelRange;
	dt0exact = sqrt(2 / maxAccelSteps);
	// divide by 2 because we want to split the waiting time per step between the rising
	// and the falling edge of the step, also convert to us
	dt0 = 0.676 * dt0exact / 2 * 1e6;
	// calculate minimum relative velocity
	float tmpMinVelRel = 1 / (0.676 * dt0exact * maxVelocitySteps);
	// use as lower limit if it is larger than current value
	if(tmpMinVelRel > velRelLimits[0]) velRelLimits[0] = tmpMinVelRel;

	resetState();
}


/*
Set maximum relative velocity for this motor.

INPUT:
	maxVelRel	- maximum relative velocity, value between minimum relative velocity and 1

RETURN:
	0 or error code
*/
byte StepperMotor::setMaxVelRel(float maxVelRel) {
	if((maxVelRel < velRelLimits[0]) || (maxVelRel > 1)) return errInvalidArg;
	velRelLimits[1] = maxVelRel;
	return errSuccess;
}


/*
Set minimum position in steps for this motor.

INPUT:
	minPos	- minimum position in steps
RETURN:
	0 or error code
*/
byte StepperMotor::setMinPos(int minPos) {
	if((minPos < 0) || (minPos >= posLimits[1])) return errInvalidArg;
	posLimits[0] = minPos;
	return errSuccess;
}


/*
Set maximum position in steps for this motor.

INPUT:
	maxPos	- maximum position in steps
RETURN:
	0 or error code
*/
byte StepperMotor::setMaxPos(int maxPos) {
	if((maxPos > stepsPerMM * travelRange) || (maxPos <= posLimits[0])) return errInvalidArg;
	posLimits[1] = maxPos;
	return errSuccess;
}


/*
Store all the available information about the motor and its current state in the struct that is passed as a parameter.
*/
void StepperMotor::getInfo(InfoMsg *info) {
	float currentVel = getCurrentVel();

	info->currentPosSteps = currentPos;
	info->currentVelRel = currentVel / maxVelocitySteps;
	info->currentPosMM = currentPos / stepsPerMM;
	info->currentVelMM = currentVel / stepsPerMM * 1e6;
	info->homed = homed;
	info->stepsPerMM = stepsPerMM;
	info->minVel = velRelLimits[0] * maxVelocity;
	info->maxVel = maxVelocity;
	info->maxAccel = maxAccel;
	info->minPosSteps = posLimits[0];
	info->maxPosSteps = posLimits[1];
	info->minVelRel = velRelLimits[0];
	info->maxVelRel = velRelLimits[1];
	info->lastHomeOffset = lastHomeOffset;
}


/*
Shorter version of getInfo() that just returns the position in Steps and the relative velocity. Use this
for regular polling.
*/
void StepperMotor::getStatus(StatusMsg *status) {
	float currentVel = getCurrentVel();
	status->pos = currentPos;
	status->velRel = currentVel / maxVelocitySteps;
}


/*
Return a string with some status information for printing out during debugging.

RETURN:
	String object
*/
String StepperMotor::getDebugInfo() {
	String term = "\n";
	String info = "N: " + String(N) + term;
	info += "currentPos: " + String(currentPos) + term;
	info += "currentDt: " + String(currentDt) + term;
	info += "ccwLS: " + String(ccwLSBuffer.state()) + ", cwLS: " + String(cwLSBuffer.state()) + term;
	info += "motionTarget: " + String(motionTarget) + ", motionType: " + String(motionType) + ", recoveryType: " + String(recoveryType) + term;
	info += "nSteps: " + String(nSteps) + ", targetPos: " + String(targetPos) + ", targetN: " + String(targetN) + term;
	info += "currentDir: " + String(currentDir) + ", targetDir: " + String(targetDir) + ", dStep: " + String(dStep) + term;
	return info;
}


/*
Digital write function that is faster than the default digitalWrite of the Arduino library. Saves ~ 2 us per write.
Found at http://forum.arduino.cc/index.php?topic=129868.15

NOTE:
	Works only on the Arduino Due and might need replacement on other platforms!

INPUT:
	pin - pin number
	val	- HIGH or LOW
*/
inline void StepperMotor::digitalWriteDirect(int pin, bool val){
  if(val) g_APinDescription[pin].pPort -> PIO_SODR = g_APinDescription[pin].ulPin;
  else    g_APinDescription[pin].pPort -> PIO_CODR = g_APinDescription[pin].ulPin;
}


/*
Faster digitalRead replacement

NOTE:
	Works only on the Arduino Due and might need replacement on other platforms!

INPUT:
	pin	- pin number
*/
inline bool StepperMotor::digitalReadDirect(int pin){
  return !!(g_APinDescription[pin].pPort -> PIO_PDSR & g_APinDescription[pin].ulPin);
}


/*
Set the direction of the motor.

INPUT:
	direction - false/LOW for forward (extruding the screw), true/HIGH for backward (retracting the screw)
*/
void StepperMotor::setDirection(bool direction) {
	digitalWriteDirect(dirPin, direction);
	currentDir = direction;
	if(direction == FORWARD) dStep = 1;
	else dStep = -1;
}


/*
Check if any of the limit switches is reached.
The limit switches are assumed to be HIGH while everything is fine and LOW when the limit is exceeded.
Note that ring buffers are used here for the limit switches because noise in the digital readout lead to
errors in detecting if the limits were hit. Since the buffers are updated only when the motor is stepping,
the motor will run a few steps (half the buffer size) beyond the limit switch which I believe to be fine
but could potentially be a problem with other motors.

RETURN:
	'true'	- if a limit switch is triggered
	'false'	- position is within the limits
*/
bool StepperMotor::checkHWLimits() {
	ccwLSBuffer.enqueue(digitalReadDirect(ccwLimitPin) == LIMIT_SWITCH_TRIGGERED);
	cwLSBuffer.enqueue(digitalReadDirect(cwLimitPin) == LIMIT_SWITCH_TRIGGERED);

	return ccwLSBuffer.state() || cwLSBuffer.state();
}


/*
Check the software limits for the position. Note that this is only performed when the motor was homed properlyy
('homed' variable is true).

RETURN:
	'true'	- if a limit is exceeded
	'false'	- position is within the limits or the motor is not homed
*/
bool StepperMotor::checkSWLimits() {
	if(homed && ((currentPos < posLimits[0]) || (currentPos > posLimits[1]))) {
		return true;
	}
	return false;
}


/*
Check if the given position is within the software position limits. These can be used as a safety margin
before the limit switches or if no limit switches are present.

NOTE:
	This is not checked after every step, so for the velocity mode, don't rely on this!!!

RETURN:
	coerced position value
*/
int StepperMotor::coercePosition(int position) {
	// check position for errors and adjust target position
	if(position < posLimits[0]) return posLimits[0];
	else if(position > posLimits[1]) return posLimits[1];
	else return position;
}


/*
Check if the given velocity is within the software limits.

RETURN:
	coerced velocity translated to steps / s
*/
float StepperMotor::coerceVelocity(float velocity) {
	// check velocity for errors and adjust target velocity
	// velocity should be relative, i.e. betwenn 0 and 1
	float vel;
	if(velocity == 0) vel = 0;
	else if(velocity < velRelLimits[0]) vel = velRelLimits[0];
	else if(velocity > velRelLimits[1]) vel = velRelLimits[1];
	else vel = velocity;
	return vel * maxVelocitySteps;
}


/*
Turn the motor off on and set the direction pin.

INPUT:
	direction	- boolean value to set the motor direction
*/
void StepperMotor::turnMotorOn() {
	// turn motor on
	digitalWriteDirect(disPin, LOW);
	// store new state
	motorOn = true;
}


/*
Turn the motor off.
*/
void StepperMotor::turnMotorOff() {
	// turn motor off
	digitalWriteDirect(disPin, HIGH);
	motorOn = false;
}


/*
Reset the velocity profile vector. Use this when interrupting the execution of a profile to erase the leftovers
of the previous one.
*/
void StepperMotor::resetState() {
	recoveryType = MOTION;
	motionTarget = POSITION;
	motionType = NONE;
	nSteps = 0;
	currentDt = 0;
	N = -1;
	lastStepMicros = 0;
	stepState = LOW;
	digitalWriteDirect(stepPin, LOW);
}


/*
Compute N that corresponds to a given velocity. Note that this will almost certainly floor() the velocity.

INPUT:
	velocity	- velocity in steps / s

RETURN:
	N
*/
int StepperMotor::getN(float velocity) {
	// N that is closest to the given velocity
	// N = (v^2) / (2 * a0)
	if(velocity == 0) return -1;
	else return (velocity * velocity) / (2 * maxAccelSteps);
}


/*
Same as "getN" but tages a realtive velocity as input which is checked using "coerceVelocity".

INPUT:
	velocity	- relative velocity

RETURN:
	N
*/
int StepperMotor::getNFromVelRel(float velocityRel) {
	return getN(coerceVelocity(velocityRel));
}


/*
Get the current velocity in steps / us.

RETURN:
	velocity in steps / s
*/
float StepperMotor::getCurrentVel() {
	// testing for N == -1 is a reliable check for zero velocity (better than comparing floats)
	if(N == -1) {
		return 0;
	} else {
		// use 0.5 because currentDt is only half the time required for a step
		// (due to having a rising and a falling edge per step and splitting the waiting time between them)
		// and convert from us to s
		// dStep converts to positive or negative velocity
		return dStep * 5e5 / currentDt;
	}
}


/*
Check if the motor should be stepped (waiting time is between steps is over).

RETURN:
	0 if it was not time to step, output of doStep() otherwise
*/
byte StepperMotor::checkStep() {
	if((motionType != NONE) && (micros() - lastStepMicros >= currentDt))  {
		return doStep();
	} else return errNoStepPending;
}


/*
Do the next step of the motor.

RETURN:
	0 or error code
*/
byte StepperMotor::doStep() {

	lastStepMicros = micros();

	// if stepState is low, we're at the rising edge of the signal
	if(!stepState) {
		// safety check to prevent "infinite" stepping due to integer underflow
		if(nSteps == 0) return errStepUnderflow;

		// check for limits before stepping if applicable
		#ifdef USE_LIMITS
		if(recoveryType != HW_RECOVERY) {
			if(checkHWLimits()) return doHWRecovery();
			else if((recoveryType != SW_RECOVERY) && (!homing)) {
				// don't check sw limits when recovering or homing the motor
				if(checkSWLimits()) return doSWRecovery();
			}
		}
		#endif

		// GeckoDrive steps on rising edge
		digitalWriteDirect(stepPin, HIGH);
		stepState = HIGH;

		// compute new N and dt depending on movement type
		// don't do anything for constant movement
		switch(motionType){
			case ACCELERATION:
				if(N == -1) {
					currentDt = dt0;
					N = 0;
				} else {
					currentDt = currentDt - 2 * currentDt / (4 * ++N + 1);
				}
				break;
			case DECELERATION:
				if(N == 1) {
					currentDt = dt0;
					N = 0;
				} else {
					currentDt = currentDt + 2 * currentDt / (4 * --N + 1);
				}
				break;
		}

		// position counter
		currentPos += dStep;
		// substract a step
		nSteps--;
	} else {
		// otherwise we're at the falling edge
		digitalWriteDirect(stepPin, LOW);
		stepState = LOW;

		// did we reach the end of the current section?
		if(nSteps == 0) {
			// check what to do next
			if(recoveryType == HW_RECOVERY) {
				checkHWLimits();
				return doHWRecovery();
			}
			if(recoveryType == SW_RECOVERY) return doSWRecovery();
			if(motionTarget == POSITION) return doMoveTo();
			if(motionTarget == VELOCITY) return doMoveVelocity(); 
		}
	}

	return errSuccess;
}


/*
Brings the motor to a stop by decelerating linearly with maximum acceleration.
*/
void StepperMotor::moveToStop() {
	motionTarget = POSITION;
	motionType = DECELERATION;
	// are we moving with min speed? if so, just stop
	if(N == 0) {
		resetState();
		return;
	}
	// get number of steps to stop
	nSteps = N;
	// adjust targetPos
	if(currentDir == FORWARD) {
		targetPos = currentPos + nSteps;
	} else {
		targetPos = currentPos - nSteps;
	}
}

	
/*
Command the motor to a position in steps.

INPUT:
	position	- position within the motor range in steps
	velocity	- relative velocity of the motion

RETURN:
	output of doMoveTo
*/
byte StepperMotor::moveTo(int position, float velocity /* = 1 */) {
	// if the motor is not homed, motion is potentially unsafe so return an error
	if(!homed) return errNotHomed;
	// check position for errors and adjust target position
	targetPos = coercePosition(position);
	// get target N (velocity index)
	// velocity should be relative, i.e. betwenn 0 and 1
	targetN = getNFromVelRel(velocity);
	if(targetN == -1) return errInvalidArg;
	return doMoveTo();
}


/*
Perform the number of steps given. A linear acceleration is used (equivalent to sqrt-velocity vs step profile).

NOTE: This function only accepts input if the motor is not moving.

INPUT:
	steps		- number of steps, positive or negative to set the direction
	velocity	- relative velocity (between 0 and 1) of maxVelocity

RETURN:
	output of moveTo
*/
byte StepperMotor::moveSteps(int steps, float velocity) {
	// if the motor is not homed, motion is potentially unsafe so return an error
	if(!homed) return errNotHomed;
	// check if we are already moving, if so return a not implemented error
	if (N != -1) {
		return errNotImplemented;
	}
	return moveTo(currentPos + steps, velocity);
}


/*
Calculates the next motion sequence if motionType is POSITION by using targetPos and targetVel.

RETURN:
	0 or error code
*/
byte StepperMotor::doMoveTo() {
	motionTarget = POSITION;

	// get distance to target
	int steps = targetPos - currentPos;
	// are we there already?
	if(steps == 0) {
		resetState();
		return errTargetReached;
	} else if(steps > 0) {
		targetDir = FORWARD;
	} else {
		targetDir = BACKWARD;
		steps = -steps;
	}


	/****** get movement parameters ******/
	// adjust direction
	if(targetDir != currentDir) {
		// using N to check for zero velocity is more reliable than comparing floats
		if(N <= 0) {
			// flip direction
			setDirection(targetDir);
		} else {
			// we're moving in the wrong direction, come to a stop
			motionType = DECELERATION;
			nSteps = N;
			return errSuccess;
		}
	}

	// are we close or would we overshoot the target (= too fast now)
	// (N = number of steps required to stop)
	if(N >= steps) {
		motionType = DECELERATION;
		nSteps = N;
		return errSuccess;
	}

	// are we moving at the right speed? or do we only need one spacer step?
	// then keep on going
	if((N == targetN) || (steps - N == 1)) {
		motionType = CONSTANT;
		// adjust movement when close to the target
		nSteps = steps - N;
		return errSuccess;
	}

	// are we moving too slow? accelerate!
	if(N < targetN) {
		motionType = ACCELERATION;
		// get nSteps and make sure we don't accelerate too much
		// case where we will reach maximum velocity (targetN)
		nSteps = targetN - N;
		// case of not reaching max velocity, we're too close to the target
		int stepsTmp = (steps - N) / 2;
		// choose smaller nSteps
		if(stepsTmp <= nSteps) nSteps = stepsTmp;
		return errSuccess;
	}

	// are we moving too fast? decelerate!
	if(N > targetN) {
		motionType = DECELERATION;
		// get number of steps to reach target velocity
		// requires targetN not to be -1 which would not make sense anyway
		nSteps = N - targetN;
		return errSuccess;
	}

	return errSuccess;
}


/*
Command the motor to move with the given velocity in the given direction.

INPUT:
	velocity	- relative velocity (is checked by coerceVelocity)
	direction	- HIGH or LOW to set the direction
*/
byte StepperMotor::moveVelocity(float velocity) {
	// if the motor is not homed, motion is potentially unsafe so return an error
	if(!homed) return errNotHomed;
	// adjust target direction depending on 'velocity' sign
	if(velocity >= 0) {
		targetDir = FORWARD;
		if(currentPos >= posLimits[1]) return errInvalidAtPos;
	} else {
		targetDir = BACKWARD;
		velocity = -velocity;
		if(currentPos <= posLimits[0]) return errInvalidAtPos;
	}
	targetN = getNFromVelRel(velocity);
	return doMoveVelocity();
}


/*
Calculates the next motion sequence if motionType is VELOCITY by using targetVel and targetDir.

RETURN:
	0 or error code
*/
byte StepperMotor::doMoveVelocity() {
	motionTarget = VELOCITY;

	// check exit condition
	if((targetN == -1) && (N <= 0)) {
		// we have to exit if N = 0 is reached and the target is to stop
		resetState();
		return errSuccess;
	}

	/****** get movement parameters ******/
	// adjust direction
	if(targetDir != currentDir) {
		// using N to check for zero velocity which can be N = 0 (was just decelerating) or N = -1 (was stopped before)
		if(N <= 0) {
			// flip direction
			setDirection(targetDir);
		} else {
			// we're moving in the wrong direction, come to a stop
			motionType = DECELERATION;
			nSteps = N;
			return errSuccess;
		}
	}

	// are we moving too slow? accelerate!
	if(N < targetN) {
		motionType = ACCELERATION;
		// get nSteps and make sure we don't accelerate too much
		nSteps = targetN - N;
		return errSuccess;
	}

	// are we moving at the right speed? then keep on going
	if(N == targetN) {
		motionType = CONSTANT;
		nSteps = -1;		
		return errSuccess;
	}

	// are we moving too fast? decelerate!
	if(N > targetN) {
		motionType = DECELERATION;
		// get number of steps to reach target velocity
		if(targetN == -1) nSteps = N;
		else nSteps = N - targetN;
		return errSuccess;
	}

	return errSuccess;
}


/*
If the hardware limit switches were hit, slowly move back within the safe region. Also takes care of homing.

RETURN:
	0 or error code
*/
byte StepperMotor::doHWRecovery() {
	recoveryType = HW_RECOVERY;
	motionType = CONSTANT;
	currentDt = dt0;
	N = 0;
	nSteps = 1;

	// figure out which limit was exceeded and set options accordingly
	if(cwLSBuffer.state()) {
		setDirection(BACKWARD);
		return errRecoveryHW;
	} else if(ccwLSBuffer.state()) {
		setDirection(FORWARD);
		return errRecoveryHW;
	} else {
		if(homed) {
			// no homing requested
			resetState();
			return errSuccess;
		} else {
			if(homing) {
				// first step of homing done (finding the limit switch)
				// second step: move offset away from it
				targetN = 0;
				targetPos = currentPos + HOME_LS_OFFSET;
				homing = false;
				return doMoveTo();
			} else {
				// homing done, store new values and return
				lastHomeOffset = currentPos;
				currentPos = 0;
				homed = true;
				resetState();
				ccwLSBuffer.reset();
				cwLSBuffer.reset();
				return errHomeSuccess;
			}
		}
	}
}


/*
If the software position limits were exceeded, move back within the boundaries smoothly (proper deceleration
and acceleration).

RETURN:
	0 or error code
*/
byte StepperMotor::doSWRecovery() {
	recoveryType = SW_RECOVERY;

	if(currentPos < posLimits[0]) {
		return moveTo(posLimits[0], SW_RECOVERY_VELOCITY);
	} else if(currentPos > posLimits[1]) {
		return moveTo(posLimits[1], SW_RECOVERY_VELOCITY);
	} else {
		resetState();
		return errSuccess;
	}
}


/*
Trigger the homing of the motor. It starts by moving the motor back slowly which will trigger the
recovery mode as soon as the hardware limit is reached, software limits are ignored since they are
considered non-calibrated.
Use this when turning on the Arduino to make sure the motors are set correctly.

RETURN:
	0 or error code
*/
byte StepperMotor::moveHome() {
	#ifdef USE_LIMITS
		homed = false;
		homing = true;
		targetDir = BACKWARD;
		targetN = getNFromVelRel(0.04);
		return doMoveVelocity();
	#else
		// there is no homing without a hardware identifier of the home position
		return errNotImplemented;
	#endif
}


/*
Immediately stop the motor. To avoid loosing steps, use 'moveVelocity(0)' for stopping. This is more of an
emergency function.

RETURN:
	0 or error code
*/
byte StepperMotor::stop() {
	resetState();
	return errSuccess;
}