/********************************************************************************

The MIT License (MIT)

Copyright (c) 2015 Christoph Ehrlich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

********************************************************************************/


#ifndef StepperMotor_h
#define StepperMotor_h

#include <Arduino.h>
#include "Config.h"
#include "ErrorCodes.h"
#include "BoolBuffer.h"


// using the packed attribute here to prevent padding bytes for memory alignment
// this is important when using these structs e.g. in serial communication (see SerialMotorController)
struct __attribute__ ((packed)) StatusMsg{
	int pos;
	float velRel;
};

struct __attribute__ ((packed)) InfoMsg{
	int currentPosSteps;
	float currentVelRel;
	float currentPosMM;
	float currentVelMM;
	bool homed;
	float stepsPerMM;
	float minVel;
	float maxVel;
	float maxAccel;
	int minPosSteps;
	int maxPosSteps;
	float minVelRel;
	float maxVelRel;
	int lastHomeOffset;
};

class StepperMotor {
public:
	/****** helper variables ******/
	// max velocity in steps / s
	float maxVelocitySteps;

	/****** methods ******/
	// helper methods
	inline void digitalWriteDirect(int pin, bool val);
	inline bool digitalReadDirect(int pin);

	//configuration methods
	void configPins(int _disPin,
					int _dirPin,
					int _stepPin,
					int _ccwLimitPin,
					int _cwLimitPin
					);
	void configMotor(float _maxVelocity,
					 float _maxAccel,
					 float _stepsPerMM,
					 float _travelRange = 0	// default values in case no limits are required
					 );
	byte setMaxVelRel(float maxVelRel);
	byte setMinPos(int minPos);
	byte setMaxPos(int maxPos);
	
	// status methods
	void getInfo(InfoMsg *info);
	void getStatus(StatusMsg *status);
	String getDebugInfo();

	// movement methods
	byte checkStep();
	byte doStep();
	void moveToStop();
	byte moveSteps(int steps, float velocity);
	byte moveTo(int position, float velocity = 1);
	byte moveVelocity(float velocity);
	byte moveHome();
	byte stop();

private:
	/****** config variables ******/
	// LED pin
	const int ledPin = 13;
	// disable pin of the motor controller
	int disPin;
	// direction pin
	int dirPin;
	// step pin
	int stepPin;
	// ccw limit switch pin
	int ccwLimitPin;
	// cw limit switch pin
	int cwLimitPin;
	// max travel range in mm
	float travelRange;
	// steps per mm travel
	// this depends on the motor (lead screw + gearbox) and the controller (usteps / full step)
	float stepsPerMM;
	// max velocity in mm/s
	float maxVelocity;
	// max acceleration in mm/s^2
	float maxAccel;

	/****** helper variables ******/
	// max acceleration in steps / s^2
	float maxAccelSteps;
	// minimum and maximum position of the motor in terms of controller steps
	// lower limit could be changed here but other sections in the code might assume this to be 0, check it!
	int posLimits[2];
	// limits for relative velocity, lower limit is adjusted to dt0 if it is smaller than that
	float velRelLimits[2] = {0, 1};
	// acceleration constant for motor control
	float dt0exact;
	// acceleraton constant fixed for initial step in us
	float dt0;

	/****** state variables ******/
	// current motor state
	bool motorOn = false;
	// was the motor homed?
	bool homed = false;
	// is the homing in process?
	bool homing = false;
	// what is the current target? reach a given position or a velocity?
	enum MotionTarget {STEPS, POSITION, VELOCITY};
	MotionTarget motionTarget = POSITION;
	// which type of movement are we currently doing?
	// HW and SW are for RECOVERY mode and determine which limit was exceeded
	enum MotionType {NONE, ACCELERATION, CONSTANT, DECELERATION};
	MotionType motionType = NONE;
	// handle violation of different limits respectively
	enum RecoveryType {MOTION, SW_RECOVERY, HW_RECOVERY};
	RecoveryType recoveryType = MOTION;
	// target position in POSITION case
	int targetPos = 0;
	// target N: this acts as an integer index for the velocity (used for POSITION and VELOCITY)
	int targetN = 0;
	// target direction in VELOCITY case
	bool targetDir = FORWARD;
	// number of steps for current movement
	unsigned int nSteps = 0;
	// step counter for waiting time computation
	int N = -1;
	// current position of motor
	int currentPos = 0;
	// waiting time in us
	float currentDt = 0;
	// change in position when doing one step (usually +/- 1)
	int dStep = 1;
	// current direction pin setting
	bool currentDir = FORWARD;
	// time of last step
	unsigned long lastStepMicros = 0;
	// is the step pin currently HIGH or LOW? so what should we do next
	bool stepState = LOW;
	// ring buffers for the hardware limit switches
	BoolBuffer<LIMIT_SWITCH_BUFFER_SIZE> ccwLSBuffer;
	BoolBuffer<LIMIT_SWITCH_BUFFER_SIZE> cwLSBuffer;
	// offset between expected and found home position
	int lastHomeOffset = 0;

	/***** methods ******/
	void setDirection(bool direction);
	bool checkHWLimits();
	bool checkSWLimits();
	int coercePosition(int position);
	float coerceVelocity(float velocity);
	void turnMotorOn();
	void turnMotorOff();
	void resetState();
	int getN(float velocity);
	int getNFromVelRel(float velocityRel);
	float getCurrentVel();
	byte doMoveTo();
	byte doMoveVelocity();
	byte doHWRecovery();
	byte doSWRecovery();
};

#endif