/********************************************************************************

The MIT License (MIT)

Copyright (c) 2016 Christoph Ehrlich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

********************************************************************************/

#ifndef SerialMotorController_h
#define SerialMotorController_h

#include <Arduino.h>
#include "Config.h"
#include "ErrorCodes.h"
#include "StepperMotor.h"


class SerialMotorController{
public:
	SerialMotorController(Stream &port);
	StepperMotor motors[NUMBER_MOTORS];

	void setup();
	void step();
	void serialListen();

private:
	Stream &_port;

	/*** serial communication ***/
	// termination byte when receiving a command and sending a reply
	byte termByte = 10;
	// time to listen on the serial port per loop iteration in us
	unsigned long serialListenTime = 50;
	// position of the next received byte in the cmd bytes array
	int cmdBufferPos = 0;
	// timeout counter for command reception
	unsigned long cmdStartTime = 0;
	// timeout for command reception in us
	unsigned long cmdTimeout = 
	#ifdef SERIAL_DEBUG
		1000000
	#else
		20000
	#endif
		;


	// serial command structure
	// note that these unions rely on the structures having no padding bytes due to automatic memory alignment
	// the structs therefore have to use the packed attribute
	union {
		struct __attribute__ ((packed)) {
			byte cmd;		// actual command
			byte id;		// motor id
			int ival;		// integer parameter
			float fval;		// float parameter
		} msg;
		byte bytes[sizeof(msg)];
	} cmd;

	// motor status structure
	union {
		StatusMsg msg[NUMBER_MOTORS];
		byte bytes[sizeof(msg)];
	} statusPckg;

	// motor info structure
	union {
		InfoMsg msg[NUMBER_MOTORS];
		byte bytes[sizeof(msg)];
	} infoPckg;

	void serialReply(byte byteArray[], unsigned int size);
	void serialReply(byte byteReply);
	void serialCallHandler();

	/*** motor control stuff ***/
	void serialUnknownCommand();
	byte checkMotorId();
	void serialStatus();
	void serialInfo();
	void serialMoveSteps();
	void serialMoveTo();
	void serialMoveVelocity();
	void serialMoveStop();
	void serialHome();
	void serialSetMaxVelRel();
	void serialSetMinPos();
	void serialSetMaxPos();
};

#endif