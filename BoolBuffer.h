/********************************************************************************

This is an implementation of a ring buffer to keep track of the limit switch state
and get the average limit switch value of the last visited positions.


The MIT License (MIT)

Copyright (c) 2015 Christoph Ehrlich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

********************************************************************************/


/*
Ring buffer implementation to keep track of the limit switch state.
*/

template<int size>
class BoolBuffer {
public:
	BoolBuffer();

	void enqueue(int newElement);
	void reset();
	bool state();
	int sum();

private:
	int n;
	int data[size];
	int _sum;
	int limit = size * 0.8;
};

template<int size>
BoolBuffer<size>::BoolBuffer() {
	reset();
}

template<int size>
void BoolBuffer<size>::enqueue(int newElement) {
	if(n >= size) n = 0;
	_sum -= data[n];
	data[n++] = newElement;
	_sum += newElement;
}

template<int size>
bool BoolBuffer<size>::state() {
	// if limit is exceeded, the buffer returns true, otherwise false
	return _sum > limit;
}

template <int size>
void BoolBuffer<size>::reset() {
	n = 0;
	// set it to all true
	for(int i = 0; i < size; i++) {
		data[i] = 0;
	}
	_sum = 0;
}

template<int size>
int BoolBuffer<size>::sum() {
	return _sum;
}