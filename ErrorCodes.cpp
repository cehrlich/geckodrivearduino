/********************************************************************************

The MIT License (MIT)

Copyright (c) 2015 Christoph Ehrlich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

********************************************************************************/


#include "ErrorCodes.h"

// NOTE: everything < 20 is considered successful

/*** success ***/
// success
const byte errSuccess = 0;
// target reached (position, velocity etc.)
const byte errTargetReached = 1;
// homing finished
const byte errHomeSuccess = 2;
// no step pending (within waiting time)
const byte errNoStepPending = 3;

/*** general errors ***/
// general error
const byte errGeneral = 20;
// feature not implemented
const byte errNotImplemented = 21;
// invalid argument
const byte errInvalidArg = 22;

/*** motion errors ***/
// motor is not homed
const byte errNotHomed = 30;
// hardware limit switch triggered
const byte errRecoveryHW = 31;
// number of steps to make is 0, risk of integer underflow
const byte errStepUnderflow = 32;
// command invalid at current position
const byte errInvalidAtPos = 33;

/*** serial errors ***/
// general serial error
const byte errSerialGeneral = 100;
// unknown command
const byte errSerialUnknown = 102;
// general argument error
const byte errSerialInvalidArg = 103;
// invalid motor id given
const byte errSerialId = 104;