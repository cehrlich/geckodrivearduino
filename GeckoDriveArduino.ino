/*******************************************************************************
Motor controller example. It was developped using a Thorlabs stepper motor
with a GeckoDrive controller. It could also work for other motor / driver 
combinations but I don't guarantee that this is the case. Make sure you check
the code and the comments before.

USAGE:
	Define the number of motors in Config.h, then follow this example.
********************************************************************************/

#include "Config.h"
#include <Arduino.h>
#include "SerialMotorController.h"
#include "StepperMotor.h"

SerialMotorController *controller;
int i = 0;

void setup() {	
	// using native USB port for speed, the baud rate is ignored
	SerialUSB.begin(115200);

	#ifdef SERIAL_DEBUG
		DEBUG_PORT.begin(115200);

		// wait for serial connection, used for debugging
		while(!DEBUG_PORT) {;}
		DEBUG_PORT.println("welcome");
	#endif

	controller = new SerialMotorController(SerialUSB);
	// configure motors
	// configPins(disPin, dirPin, stepPin, ccwLimitPin, cwLimitPin)
	controller->motors[0].configPins(52, 50, 48, 44, 46);
	// configMotor(maxVelocity, maxAccel, stepsPerMM, travelRange = 0)
	controller->motors[0].configMotor(2.0, 10.0, 9807.7366, 13.0);

	// configPins(disPin, dirPin, stepPin, ccwLimitPin, cwLimitPin)
	controller->motors[1].configPins(42, 40, 38, 34, 36);
	// configMotor(maxVelocity, maxAccel, stepsPerMM, travelRange = 0)
	controller->motors[1].configMotor(2.0, 10.0, 9807.7366, 13.0);

	// configPins(disPin, dirPin, stepPin, ccwLimitPin, cwLimitPin)
	controller->motors[2].configPins(32, 30, 28, 24, 26);
	// configMotor(maxVelocity, maxAccel, stepsPerMM, travelRange = 0)
	controller->motors[2].configMotor(2.0, 10.0, 9807.7366, 13.0);

	// configPins(disPin, dirPin, stepPin, ccwLimitPin, cwLimitPin)
	controller->motors[3].configPins(23, 25, 27, 31, 29);
	// configMotor(maxVelocity, maxAccel, stepsPerMM, travelRange = 0)
	controller->motors[3].configMotor(2.0, 10.0, 9807.7366, 13.0);

	// controller setup routine
	controller->setup();
}

void loop() {
	controller->serialListen();
	controller->step();
}
