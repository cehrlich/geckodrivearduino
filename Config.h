#ifndef Config_h
#define Config_h

/******** CONFIG ********/
// uncomment the following lines for debugging output on the serial monitor
// # define SERIAL_DEBUG
// # define DEBUG_PORT Serial

#define NUMBER_MOTORS 4

// set values for the direction pin that are used as "forward" or "backward" direction
/* Note that "positive direction" is defined as motion from the ccw limit switch towards
the cw limit switch in this code. This is important when using the limit switches and
it should be double checked that this is set correctly.
Otherwise the motor might enter recovery mode moving into the wrong direction which
will damage the motor! */
#define FORWARD HIGH
#define BACKWARD !FORWARD

// uncomment this line if you do not wish to use the hardware (limit switches) and software limits
#define USE_LIMITS

// limit switch buffer size
#define LIMIT_SWITCH_BUFFER_SIZE 20
// limit switch trigger value
// when this value is returned by digitalRead, the limit switch is considered as being hit
#define LIMIT_SWITCH_TRIGGERED LOW
#define LIMIT_SWITCH_NOT_TRIGGERED !LIMIT_SWITCH_TRIGGERED
// distance of home (0) position from ccw limit switch
#define HOME_LS_OFFSET 1000

// relative velocity to use during software recovery
#define SW_RECOVERY_VELOCITY 0.05

/****** END CONFIG ******/
#endif